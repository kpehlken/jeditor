import React, { Component } from 'react';
import './Lines.css';

class Lines extends Component {

  constructor(props) {
    super(props);

    this.formatLines = this.formatLines.bind(this);
    this.textArea = React.createRef();
  }

  formatLines() {
    let lineStr = "";
    for(let i = 1; i <= this.props.amount; i++) {
      lineStr += i + "\n";
    }
    return lineStr;
  }

  render() {
    if(this.props.exceededHeight) {
      this.textArea.current.scrollTop = this.textArea.current.scrollHeight;
    }

    return (
      <textarea
       ref={ this.textArea }
       value={ this.formatLines() }
       onChange={ this.handleChange }
       readOnly={ true }
       className="lines"
       />
    );
  }
}

export default Lines;
