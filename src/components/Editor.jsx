import React, { Component } from 'react';
import './Editor.css';
import Lines from './Lines';
import Prism from 'prismjs';
import '../Highlight.css';

class Editor extends Component {

  constructor(props) {
    super(props);

    this.state = {
          code: "",
          exceededHeight: false,
          currentLine: 1,
          maxLine: 1
    };

    this.output = React.createRef();

    this.handleChange = this.handleChange.bind(this);
    this.handleFeatures = this.handleFeatures.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  getNextChar(char, event) {
    const editor = event.target;

    if(this.state.code.substring(editor.selectionStart, editor.selectionStart + 1) === char) {
      return true;
    }
    return false;
  }

  handleClick(event) {
    const editor = event.target;

    const currentLine = editor.value.substr(0, editor.selectionStart).split("\n");

    this.setState({
      currentLine: currentLine.length
    });
  }

  handleChange(event) {
    const code = event.target.value;

    let cleanedCode = code
                      .replace(/&/g, "&amp;")
                      .replace(/</g, "&lt;")
                      .replace(/>/g, "&gt;") + "\n";

    this.output.current.innerHTML = cleanedCode;

    this.setState({
      code: code
    });
  }

  handleFeatures(event) {
    const editor = event.target;
    const oldCursorPosition = editor.selectionStart;
    const { code, currentLine, maxLine } = this.state;
    console.log("Currently on Line: ", this.state.currentLine);

    // Scroll down when scroll is necessary
    if(event.key === "Enter" && !event.shiftKey) {
      if(editor.clientHeight < editor.scrollHeight) {
        this.setState({
          exceededHeight: true
        });
      }
      if(currentLine >= maxLine) {
        this.setState({
          maxLine: currentLine + 1
        });
      }
      this.setState({
          currentLine: currentLine + 1
      });
    }

    // Tab as indentation
    if(event.key === "Tab") {
      event.preventDefault();
      // setState with callback to reposition cursor after state is updated
      this.setState({
        code: code.substring(0, editor.selectionStart) + " ".repeat(2) + code.substring(editor.selectionStart)
      }, () => {
        editor.selectionEnd = oldCursorPosition + 2;
      });
    }

    // Curly Braces auto closing
    if(event.key === "{") {
      event.preventDefault();
      // setState with callback to reposition cursor after state is updated
      this.setState({
        code: code.substring(0, editor.selectionStart) + "{}" + code.substring(editor.selectionStart)
      }, () => {
        editor.selectionEnd = oldCursorPosition + 1;
      });
    }

    // Normal Braces auto closing
    if(event.key === "(") {
      event.preventDefault();

      // setState with callback to reposition cursor after state is updated
      this.setState({
        code: code.substring(0, editor.selectionStart) + "()" + code.substring(editor.selectionStart)
      }, () => {
        editor.selectionEnd = oldCursorPosition + 1;
      });
    }

    // Square Brackets auto closing
    if(event.key === "[") {
      event.preventDefault();
      // setState with callback to reposition cursor after state is updated
      this.setState({
        code: code.substring(0, editor.selectionStart) + "[]" + code.substring(editor.selectionStart)
      }, () => {
        editor.selectionEnd = oldCursorPosition + 1;
      });
    }

    // Quotes "" auto closing
    if(event.key === "\"") {
      event.preventDefault();
      // If "-char is next skip and move cursor + 1
      if(this.getNextChar("\"", event)) {
        editor.selectionStart = oldCursorPosition + 1;
        return;
      }
      // setState with callback to reposition cursor after state is updated
      this.setState({
        code: code.substring(0, editor.selectionStart) + "\"\"" + code.substring(editor.selectionStart)
      }, () => {
        editor.selectionEnd = oldCursorPosition + 1;
      });
    }

    // Skipping ) char
    if(event.key === ")") {
      if(this.getNextChar(")", event)) {
        event.preventDefault();
        editor.selectionStart = oldCursorPosition + 1;
      }
    }

    // Skipping } char
    if(event.key === "}") {
      if(this.getNextChar("}", event)) {
        event.preventDefault();
        editor.selectionStart = oldCursorPosition + 1;
      }
    }

    // Execute code in Browser environment
    if(event.key === "Enter" && event.shiftKey) {
      event.preventDefault();
      eval(code);  // Dangerous if used in production
    }

    // Backspace Key Functionality
    if(event.key === "Backspace") {
      // Check if selected text equals to entire code length (meaning all code has been selected)
      if(editor.selectionEnd - editor.selectionStart === code.length) {
        // Delete all code and reset state properties
        this.setState({
          currentLine: 1,
          maxLine: 1,
          exceededHeight: false,
        });
        this.handleChange(event);
      }

      if(currentLine >= 1 && code.slice(code.length - 1, code.length) === `\n`) {
        this.setState({
          currentLine: currentLine - 1,
          maxLine: maxLine - 1
        });
      }
    }

    // Arrow Up, keep track of current line
    if(event.key === "ArrowUp") {
      if(currentLine > 1) {
        const currentLine = editor.value.substr(0, editor.selectionStart).split("\n").length - 1;
        this.setState({
          currentLine
        });
      }
    }

    // Arrow Down, keep track of current line
    if(event.key === "ArrowDown") {
      if(currentLine < maxLine) {
        const currentLine = editor.value.substr(0, editor.selectionStart).split("\n").length + 1;
        this.setState({
          currentLine
        });
      }
    }
  }


  render() {
    Prism.highlightAll();

    return (
      <>
        <Lines amount={ this.state.code.split("\n").length } exceededHeight={ this.state.exceededHeight } />
        <textarea
          className="editor"
          value={ this.state.code }
          onChange={ this.handleChange }
          onKeyDown={ this.handleFeatures }
          onClick={ this.handleClick }
          >
          </textarea>
        <pre className="code-wrapper">
           <code ref={ this.output } className="language-javascript"></code>
         </pre>
      </>
    );
  }

}

export default Editor;
