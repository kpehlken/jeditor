import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import './Menu.css';


class Menu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      menuCollapsed: true
    };

    this.dropdown = React.createRef();
  }

  toggleMenu(event) {
    let menu;
    // If children is clicked set menu to parentElement
    if(event.target.classList.contains("burger")) {
      menu = event.target.parentElement;
    } else {
      menu = event.target;
    }

    // Toggle open class for Menu animation
    menu.classList.toggle("open");

    this.setState({
      menuCollapsed: !this.state.menuCollapsed
    });
  }

  render() {
    return (
      <div className="wrapper">
      <div  onClick={ (e) => this.toggleMenu(e) } className="menu">
        <div className="burger"></div>
      </div>
      <CSSTransition
        in={ !this.state.menuCollapsed }
        timeout={ 200 }
        classNames="button-group"
        unmountOnExit
        >
        <div onAnimationEnd={ this.toggleMenu } ref={ this.dropdown } className="dropdown">
          <button>Shortcuts</button>
          <button>Execute</button>
          <button>Settings</button>
        </div>
      </CSSTransition>

     </div>
    );
  }
}

export default Menu;
