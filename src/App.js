import Editor from './components/Editor';
import Menu from './components/Menu';
import './App.css';

function App() {
  return (
    <div className="container">
      <Menu />
      <Editor />
    </div>
  );
}

export default App;
